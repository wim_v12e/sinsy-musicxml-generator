# Sinsy MusicXML Generator

A simple script to generate MusicXML files of songs for Sinsy (http://sinsy.jp/), based on a plaint text input.
This script only supports Japanese songs, the lyrics must be entered in hiragana or katakana. Currently, the script supports a vocal range of three octaves.

## How to create the song input file

The song input file is a text file and must have extension `.txt`. Lines starting with `#` are comments. Blank lines are ignored. Block comments start and end with `#====`;

### MusicXML settings

At the start of the file you can provide some additional information for MusicXML in the form

    keyword  =  value

The currently supported keywords and their default values are:


- The title of the song    

      title = ''    

- The tempo

      bpm = 110

- The base octave      

      oct = 4

- The note duration

      note = 4

- The number of such notes per measure

      beats = 2

- The note division (number of divisions of a quarter note)

      divs = 4

So by default the song is in 2/4 with 1/16th note as the smallest division

- You can transpose the written music by a given number of half tones, positive for up, negative for down.

      transpose = 0

### Entering a song

Lyrics and melody must be on subsequent lines but comment and blank lines in between are allowed.

#### Lyrics

- The song lyrics _must_ be entered as katakana or hiragana. Due to a bug in Sinsy, the hiragana だ does not work, so 'da' must always be entered in katakana (ダ).
- Syllables must be separated by spaces.
- Measures must be separated by a bar (`|`).
- The underscore `_` is used for a pause.

#### Melody

- The notes must be entered as pairs of the note value and the duration, separated by a colon, for example `A:4`.
- Notes in the octave below the base octave are denoted by a `-`, e.g. 'B-:2'; notes in the octave above the base octave with a `+`, e.g. 'G+:8'.
- A sharp note is denoted by a 'k', a flat note by a 'm', and the supported values are:

      C Ck D Em E F Fk G Gk A Bm B

- Measures must be separated by a bar (`|`).       
- A pause is denoted by a 'P', e.g. 'P:3'.
- Ties are supported using `[` and `]`, for example

      B:2 C:2  [A:2 | A:4] C:4

- Sinsy requires that every song starts and ends with a pause.

## Example

As an example, here is part of a song input file I created:

    title=九月の情景

    # Tempo
    bpm=122

    # Base octave and transposition
    oct=4
    transpose=2 # two half tones

    # The Song

    ぼく  の  ま  | わり   に  | ー   _   い  | く  つもの | ろ  て  き  | が  _   | _  
    A:4   A:2 A:2 | A:4   [A:4 | A:4] P:2 A:2 | A:2 E:6    | E:2 E:2 D:4 | E:6 P:2 | P:8

    たいよー | を  さがす  | が  _
    D:8  | D:2 D:6 | E:4 P:4

    み  あ   た  | ー   ら  な   い  | ー   _   | _
    E:3 E:3 [E:2 | E:1] D:3 E:3 [E:1 | E:2] P:6 | P:8

## Creating the song with Sinsy

If you have installed Sinsy on your computer, you can create a .wav file like this:

    $ perl create_sinsy_xml.pl test_sinsy_input.txt > test_sinsy_input.xml
    $ sinsy -m nitech_jp_song070_f001.htsvoice -o test_sinsy.wav -x dic test_sinsy_input.xml

Alternatively, you can upload the MusicXML file to [the Sinsy web site](http://sinsy.jp).

#!/usr/bin/env perl
use v5.28;
use warnings;
use strict;
use utf8;
use POSIX qw(strftime);
#use feature 'unicode_strings';
use Data::Dumper;

use Carp;

binmode STDOUT, ':utf8';
#binmode STDERR, ':utf8';

if (not @ARGV)  {
    die "Please provide an input file";
}
my $input_file = $ARGV[0];#'sinsy_input.txt'
open my $IN, "<:encoding(UTF-8)", $input_file or die $!;

my @lines=();
my @attr_pairs=();
my %line_nos=();
my $line_no=0;
my $skip=0;
while (my $line=<$IN>) {
    ++$line_no;
    chomp $line;
    #    warn $line;
    $line=~s/\　/ /g; # hiragana space
    $line=~s/\　/ /g; # katakana space
    #    warn $line;
    $line=~/^\#====/ && do {
        $skip=1-$skip;
        next;
    };
    if (!$skip) {
    # skip comments and blanks
    next if $line=~/^\s*$/;
    next if $line=~/^\s*\#/;
    $line=~s/\#.+$//;
    # remove leading and trailing whitespace
    $line=~s/^\s+//;
    $line=~s/\s+$//;
    $line_nos{$line}=$line_no;
    # handle key=value pair lines
    if ($line=~/=/) {
        my ($key,$val)=split(/\s*=\s*/,$line);
        push @attr_pairs, [$key, $val];
    } else {
        push @lines, $line;
    }
    }
}
close $IN;

# Key=Value pairs supported in the input file
my $transpose=0;
my $oct_default=4;
my $bpm=110;
my $key='G';
my $divs=4; # so a quarter note is divided into 4 and a duration of 8 means two quarter notes
my $mode='major';
# The following means the song is in 2/4
my $beats=2; # How many notes
my $beat_type=4; # their duration in number of divisions
my $title='';
for my $pair ( @attr_pairs ) {
    my ($k,$val)=@{$pair};
    if ($k eq 'oct') {
        $oct_default=$val;
    }
    elsif ($k eq 'bpm') {
        $bpm=$val;
    }
    elsif ($k eq 'divs') {
        $divs=$val;
    }
    elsif ($k eq 'beats') {
        $beats=$val;
    }
    elsif ($k eq 'note') {
        $beat_type=$val;
    }
    elsif ($k eq 'key') {
        $key=$val;
    }
    elsif ($k eq 'title') {
        $title=$val;
    }
     elsif ($k eq 'transpose') {
        $transpose=$val;
    }    
}

my $date = scalar localtime;
my $iso_date = strftime "%F",localtime;
my $header=<<"ENDH";
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE score-partwise PUBLIC "-//Recordare//DTD MusicXML 2.0 Partwise//EN"
                                "http://www.musicxml.org/dtds/partwise.dtd">
<score-partwise version="2.0">
  <identification>
    <encoding>
      <software>Wim's Simple MusicXML Generator</software>
      <encoding-date>$iso_date</encoding-date>
    </encoding>
  </identification>
  <defaults>
 </defaults>
  <credit page="1">
    <credit-words xml:lang="ja">$title</credit-words>
  </credit>
  <part-list>
    <score-part id="P1">
      <part-name print-object="no">MusicXML Part</part-name>
      <score-instrument id="P1-I1">
        <instrument-name>Grand Piano</instrument-name>
      </score-instrument>
      <midi-instrument id="P1-I1">
        <midi-channel>1</midi-channel>
        <midi-program>1</midi-program>
        <volume>80</volume>
        <pan>0</pan>
      </midi-instrument>
    </score-part>
  </part-list>
  <!--=========================================================-->
  <part id="P1">

ENDH


my $footer=<<ENDF;
  </part>
  <!--=========================================================-->
</score-partwise>

ENDF

my $musescore_fudge=1;
# 16 = 4 quarts = 8 eights 
my $one_measure=$beats*($beat_type/$divs);
my $first_measure_attrs=<<"ENDFMA";
     <attributes>
        <divisions>$divs</divisions>
        <key>
          <fifths>0</fifths>
          <mode>$mode</mode>
        </key>
        <time>
          <beats>$beats</beats>
          <beat-type>$beat_type</beat-type>
        </time>
        <clef>
          <sign>$key</sign>
          <line>2</line>
        </clef>
      </attributes>
      <sound tempo="$bpm"/>

ENDFMA

my $first_measure=<<"ENDM";
    <measure number="1" width="295">
     <attributes>
        <divisions>$divs</divisions>
        <key>
          <fifths>0</fifths>
          <mode>$mode</mode>
        </key>
        <time>
          <beats>$beats</beats>
          <beat-type>$beat_type</beat-type>
        </time>
        <clef>
          <sign>$key</sign>
          <line>2</line>
        </clef>
      </attributes>
      <sound tempo="$bpm"/>
      <note>
        <rest/>
        <duration>$one_measure</duration>
        <voice>1</voice>
      </note>
    </measure>
    <!--=======================================================-->

ENDM
# duration 2, divisions 4 in a quarter, means an eighth
my %note_types = (
	4 => 'whole',
       2 => 'half',
       1 => 'quarter',
       0.5 => 'eighth',
       0.25 =>'16th',
      0.125 => '32nd'
);

my $transpose_table = mk_transpose_table($transpose);

my @scores=();
while (@lines) {
    my $lyrics = shift @lines;    
    my $music = shift @lines;
    push @scores, [$lyrics, $music] unless $lyrics eq '';
}

say $header;
say $first_measure;

my @measures=();

for my $entry (@scores) {
    my ( $lyrics, $music ) = @{$entry};
    my @syllable_measures = split(/\s*\|\s*/, $lyrics);
    my @note_measures = split(/\s*\|\s*/, $music);

# each of these is a measure with a number of syllables
my $ct=1;
for my $syl_meas_str ( @syllable_measures ) {
    ++$ct;
    say '<measure number="'.$ct.'" width="295">';
    #    if ($ct==1) {
    #	    say $first_measure_attrs;
    #}
    my $note_meas_str = shift @note_measures;
    my @syllables = split(/[\　\　\s]+/, $syl_meas_str);
    my @notes = split(/\s+/, $note_meas_str);
    my @pitch_duration = map { 
        if ($_!~/[a-gp][km]?[\+\-]?:\d+/i) {
            my $line_no=$line_nos{$music};
            die "Probably a typo: '$_' on line $line_no '$music>'";
        }
        [ split(/:/, $_) ] 
    } @notes;

    for my $entry (@pitch_duration ) {
        my ($pitch, $duration) = @{$entry};
	# 16/4 => 4 => whole 4/4 =>1 => quarter; 8/4 => 2 => half;  2/4 => 0.5 => eighth; 1/4 => 0.25=> 1=>16th 32nd
	# dots: 4 => 6; 2 => 3; 1 => 1.5; 0.5 => 0.75
	my $dot='';
	my $note_length = $duration/$divs;
	if (($note_length *4) % 3 ==0) {
		$dot='<dot/>';
		$note_length*=2/3;
		}

	my $note_type = $note_types{ $note_length};
        my $oct=$oct_default;        
        my $tie='';
        if ($pitch=~/\[/) {
           $tie= '<tie type="start"/>';
            $pitch=~s/\[//;
        }
        elsif ($pitch=~/\]/) {
            $tie = '<tie type="stop"/>';
            $pitch=~s/\]//;
        }

        if ($pitch ne 'P') {
            $pitch = $transpose_table->{$pitch};
        }

        if ($pitch=~/\+/) {$oct=$oct_default+1;
        $pitch=~s/\+//;
        }
        elsif ($pitch=~/\-/) {$oct=$oct_default-1;
        $pitch=~s/\-//;
        }
        elsif (lc($pitch) eq $pitch) {
            $oct=$oct_default-1;
            $pitch=uc($pitch);
        }

        #carp $pitch;

        my $alter='';
        if ($pitch=~/k/) {
            $pitch=~s/k//;
            $alter='<alter>+1</alter>';
        }
        elsif ($pitch=~/m/) {
            $pitch=~s/m//;
            $alter='<alter>-1</alter>';
        }


        my $syllable = shift @syllables;
        say '<note>';
        if ($pitch eq 'P') {
            say '<rest/>';
        } else {
            say '<pitch>';
          say "<step>$pitch</step>";
          say $alter unless $alter eq '';
          say "<octave>$oct</octave>";
        say '</pitch>';
        }
        say "<duration>$duration</duration>";
        say $tie unless $tie eq '';
        say '<voice>1</voice>';
        if ($syllable ne '_') {
	say '<type>'.$note_type.'</type>';
	say $dot;
          say '<lyric default-y="-77" number="1">';
          say '<syllabic>single</syllabic>';
          say '<text font-size="9.9">'.$syllable.'</text>';
        say '</lyric>';
        }
            #        say "$pitch, $duration, $syllable";
        say '</note>';
    }
     say "</measure>";
}

#my $ct=0;
#my $measure=[];
#for my $entry (@pitch_duration ) {
#my ($pitch, $duration) = @{$entry};
#my $syllable = shift @syllables;
#$ct+=$duration;
#push @{$measure}, [$pitch, $duration, $syllable];




#map {say $_ } @syllables;
#say Dumper  @pitch_duration;
}
say $footer;

sub mk_transpose_table { (my $n_half_tones) = @_;

    my @notes = qw(C- Ck- D- Em- E- F- Fk- G- Gk- A- Bm- B- C Ck D Em E F Fk G Gk A Bm B C+ Ck+ D+ Em+ E+ F+ Fk+ G+ Gk+ A+ Bm+ B+);

    my $transpose_table={};
    my $idx=0;
    for my $note (@notes) {
        my $shifted_note = $notes[$idx+$n_half_tones];
        $transpose_table->{$note}=$shifted_note;
        ++$idx;
    }
    #        croak Dumper($transpose_table);
    return $transpose_table;

} 

=pod More MusicXML features

The following musical symbols are supported: 

*tie
slur
staccato
accent
dynamics
crescendo
decrescendo
breath mark


=cut
